from django.urls import path
from .views import (
    api_list_or_create_salespeople,
    api_delete_or_update_salesperson,
    api_list_or_create_customers,
    api_delete_or_update_customer,
    api_list_or_create_sale,
    api_delete_sale,
)


urlpatterns = [
    path(
        "salespeople/",
        api_list_or_create_salespeople,
        name="api_list_or_create_salespeople",
    ),
    path(
        "salespeople/<int:id>/",
        api_delete_or_update_salesperson,
        name="api_delete_or_update_salesperson",
    ),
    path(
        "customers/", api_list_or_create_customers, name="api_list_or_create_customers"
    ),
    path(
        "customers/<int:id>/",
        api_delete_or_update_customer,
        name="api_delete_or_update_customer",
    ),
    path("sales/", api_list_or_create_sale, name="api_list_or_create_sale"),
    path("sales/<int:id>/", api_delete_sale, name="api_delete_sale"),
]
