function MainPage() {
	return (
		<div className="px-4 py-5 my-5 text-center">
			<a href="/">
				<img
					src="https://i.imgur.com/HclMof5.gif"
					title="source: imgur.com"
					alt="CarCar - The premiere solution for automobile dealership management!"
				/>
			</a>
		</div>
	);
}

export default MainPage;
