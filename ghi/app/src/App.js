import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import CreateSalesperson from "./CreateSalesperson";
import ListSalespeople from "./ListSalespeople";
import CreateCustomer from "./CreateCustomer";
import ListCustomers from "./ListCustomers";
import CreateSale from "./CreateSale";
import ListSales from "./ListSales";
import ListSalespersonHistory from "./ListSalespersonHistory";
import TechnicianForm from "./TechnicianForm";
import TechnicianList from "./TechnicianList";
import AppointmentList from "./AppointmentList";
import AppointmentForm from "./AppointmentForm";
import AppointmentHistory from "./AppointmentHistory";
import ListManufacturers from "./ListManufacturers";
import CreateManufacturer from "./CreateManufacturer";
import ListVehicleModels from "./ListVehicleModels";
import CreateVehicleModel from "./CreateVehicleModel";
import ListAutomobiles from "./ListAutomobiles";
import CreateAutomobile from "./CreateAutomobile";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route
            path="salespeople"
            element={<ListSalespeople />}
          />
          <Route path="salespeople">
            <Route path="new" element={<CreateSalesperson />} />
          </Route>
          <Route
            path="customers"
            element={<ListCustomers />}
          />
          <Route path="customers">
            <Route path="new" element={<CreateCustomer />} />
          </Route>
          <Route path="sales" element={<ListSales />} />
          <Route path="sales">
            <Route path="new" element={<CreateSale />} />
          </Route>
          <Route
            path="saleshistory"
            element={<ListSalespersonHistory />}
          />
          <Route path="/technicians/" element={<TechnicianList />} />
          <Route path="technicians/">
            <Route path="new/" element={<TechnicianForm />} />
          </Route>
          <Route path="/appointments/" element={<AppointmentList />} />
          <Route
            path="appointments/history/"
            element={<AppointmentHistory />}
          />
          <Route path="appointments/">
            <Route path="new/" element={<AppointmentForm />} />
          </Route>
          <Route
            path="manufacturers"
            element={<ListManufacturers />}
          />
          <Route path="manufacturers">
            <Route path="new" element={<CreateManufacturer />} />
          </Route>
          <Route
            path="models"
            element={<ListVehicleModels />}
          />
          <Route path="models">
            <Route path="new" element={<CreateVehicleModel />} />
          </Route>
          <Route
            path="autos"
            element={<ListAutomobiles />}
          />
          <Route path="autos">
            <Route path="new" element={<CreateAutomobile />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
