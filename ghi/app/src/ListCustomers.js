import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ListCustomers() {

  const [customers, setCustomers] = useState([]);

  async function onDelete(event, customer) {
    event.preventDefault();
    const customerUrl = `http://localhost:8090/api/customers/${customer}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  }
  const fetchData = async () => {
		const customerUrl = "http://localhost:8090/api/customers/";
		const response = await fetch(customerUrl);
		if (response.ok) {
			const data = await response.json();
			setCustomers(data.customers);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

  if (customers === undefined) {
    return null;
  }

  return (
		<>
			<table className="table table-striped table-hover">
				<thead>
					<tr>
						<th className="text-center">First Name</th>
						<th className="text-center">Last Name</th>
						<th className="text-center">Phone Number</th>
						<th className="text-center">Address</th>
					</tr>
				</thead>
				<tbody>
					{customers.map((customer) => {
						return (
							<tr key={customer.id}>
								<td className="text-center">{customer.first_name}</td>
								<td className="text-center">{customer.last_name}</td>
								<td className="text-center">{customer.phone_number}</td>
								<td className="text-center">{customer.address}</td>
								<td align="right">
									<button
										onClick={(event) => onDelete(event, customer.id)}
										className="btn btn-danger"
									>
										DELETE
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
			<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
				<Link
					to="/customers/new"
					className="btn btn-primary btn-lg px-4 gap-3"
				>
					Add a Customer
				</Link>
			</div>
		</>
	);
}

export default ListCustomers;
